//
//  ViewController.m
//  Rep
//
//  Created by Tomek Cejner on 29/10/14.
//  Copyright (c) 2014 Tomek. All rights reserved.
//

#import "ViewController.h"
#import "Injector.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"Inj: %@",  [Injector defaultInjector] );
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
