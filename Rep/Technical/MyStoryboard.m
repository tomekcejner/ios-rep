//
//  MyStoryboard.m
//  Rep
//
//  Created by Tomek Cejner on 29/10/14.
//  Copyright (c) 2014 Tomek. All rights reserved.
//

#import "MyStoryboard.h"


@implementation MyStoryboard 


- (id)instantiateViewControllerWithIdentifier:(NSString *)identifier
{
    NSLog(@"Injecting objects into UIViewController with identifier %@", identifier);
    UIViewController *controller = [super instantiateViewControllerWithIdentifier:identifier];
//    [[JSObjection defaultInjector] injectDependencies:controller];
    return controller;
}

@end
