//
// Created by Tomek Cejner on 29/10/14.
// Copyright (c) 2014 Tomek. All rights reserved.
//

#import "Injector.h"


@implementation Injector {

}

static Injector *defaultInjector;

+ (Injector *)defaultInjector {
    return defaultInjector;
}

+ (void)setDefaultInjector:(Injector *)injector {
    defaultInjector = injector;
}



@end